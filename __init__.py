from trytond.pool import Pool
from .health_relevar_fiuner import *
from .wizard import *

def register():
    Pool.register(
        relevar_import_csv.RelevarImportCsvStart,
        relevar_import_csv.RelevarImportCsvPreview,
        relevar_import_csv.RelevarImportCsvDUData,
        relevar_import_csv.RelevarImportCsvContactData,
        module='health_relevar_fiuner', type_='model')
    Pool.register(
        relevar_import_csv.RelevarImportCsvWizard,
        module='health_relevar_fiuner', type_='wizard')
