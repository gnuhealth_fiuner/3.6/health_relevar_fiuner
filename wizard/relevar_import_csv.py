from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import Not, Bool, Eval

import csv
import sys
from io import StringIO
from datetime import datetime, date, time, timedelta
from decimal import Decimal, ROUND_UP

__all__ = ['RelevarImportCsvStart',
           'RelevarImportCsvPreview',
           'RelevarImportCsvDUData',
           'RelevarImportCsvContactData',
           'RelevarImportCsvWizard']


class RelevarImportCsvStart(ModelView):
    'Relevar Import Csv Start'
    __name__ = 'gnuhealth.relevar.import_csv.start'

    file_ = fields.Binary('File', filename='name',required=True)
    name = fields.Char('Name',readonly=True)
    survey_date = fields.Date('Date of survey',required=True)
    survey_responsable =\
        fields.Char('Survey responsable', required=True,
            help="This field will be use for generate the DU code")
    country = fields.Many2One('country.country','Country',required=True)
    subdivision = fields.Many2One('country.subdivision','Province/Subdivision',required=True,
        domain=[('country','=',Eval('country',-1))],depends=['country'],
        states={
            'readonly':~Eval('country'),
            })
    city = fields.Char('City',required=True)

    @staticmethod
    def default_survey_date():
        return date.today()

    @fields.depends('file_','name')
    def on_change_with_survey_date(self, name=None):
        if self.file_:
            file_date = self.name.replace('RelevAr-','').replace('.csv','')
            try:
                survey_date = datetime.strptime(file_date,'%Y-%m-%d')
                return survey_date
            except:
                return date.today()
        return date.today()


class RelevarImportCsvPreview(ModelView):
    'Relevar Import Csv Preview'
    __name__ = 'gnuhealth.relevar.import_csv.preview'

    dus = fields.One2Many(
        'gnuhealth.relevar.import_csv.du_data',None,'DU survey data')


class RelevarImportCsvDUData(ModelView):
    'Relevar Import Csv Preview'
    __name__ = 'gnuhealth.relevar.import_csv.du_data'
    #fields related to localization
    du_code = fields.Char('DU code',readonly=True)
    du_street = fields.Char('Street', readonly=True)
    du_street_number = fields.Integer('Street number', readonly=True)
    du_latitude = fields.Char('Latitude', readonly=True)
    du_longitude = fields.Char('Longitude', readonly=True)
    du_house_number = fields.Char('House number',
        help="House number according to cartography",
        readonly=True)
    du_people = fields.One2Many(
        'gnuhealth.relevar.import_csv.contact_data',None,'People',
        readonly=True)
    du_list = fields.One2Many('gnuhealth.du',None,'Du list',readonly=True)
    check_assertion_du= fields.Boolean('Check assertion',
        help="Check if the patient data is good enough to import in case of similarities")
    du_assertion = fields.Many2One('gnuhealth.du','DU assertion',
        help="Select one assertion existent on the database to those about to import",
        domain=[('id','in', Eval('du_list'))])
    warning_icon = fields.Function(
        fields.Char('Check similarities'),'on_change_with_warning_icon')
    du_data_handle = fields.Selection ([
        ('create','Create DU with app data'),
        ('update','Update DU data with app data'),
        ('not_update','Not update data'),
        ],'Du Data handle',sort= False,
        states={
            'readonly': ~Eval('du_assertion'),
            })
    #fields related to housing conditions
    housing_minors = fields.Char('Minors', readonly=True)
    housing_adults = fields.Char('Adults', readonly=True)
    housing_type = fields.Char('Housing Type', readonly=True)
    housing_type_another = fields.Char('Housing type another', readonly=True)
    housing_bedrooms = fields.Integer('Bedrooms',readonly=True)
    housing_kitchen = fields.Char('Kitchen', readonly=True)
    housing_kitchen_chars = fields.Char('Kitchen Characteristics', readonly=True)
    housing_gas = fields.Char('Gas', readonly=True)
    housing_stove_feed = fields.Char('Stove feed', readonly=True)
    housing_walls = fields.Char('Walls', readonly=True)
    housing_walls_else = fields.Char('Walls Else', readonly=True)
    housing_floors = fields.Char('Floors', readonly=True)
    housing_floor_else = fields.Char('Floors else', readonly=True)
    housing_roofs = fields.Char('Roofs', readonly=True)
    housing_roofs_else = fields.Char('Roofs else', readonly=True)
    housing_water = fields.Char('Water', readonly=True)
    housing_water_source = fields.Char('Water source', readonly=True)
    housing_water_source_other = fields.Char('Water other', readonly=True)
    housing_water_location = fields.Char('Water source', readonly=True)
    housing_excretes = fields.Char('Excretes', readonly=True)
    housing_electricity = fields.Char('Electricity', readonly=True)
    housing_bathroom = fields.Char('Bathroom', readonly=True)
    housing_bathroom_type = fields.Char('Bathroom type', readonly=True)
    housing_snow_ice = fields.Char('Snow/Ice', readonly=True)

    @fields.depends('du_people','du_list',
        'du_assertion', 'check_assertion_du')
    def on_change_with_warning_icon(self, name=None):
        if 'gnuhealth-warning' in [x.warning_icon for x in self.du_people] or (self.du_list and not self.check_assertion_du):
            return 'gnuhealth-warning'
        elif self.check_assertion_du:
            return ''
        return ''

    @staticmethod
    def default_du_data_handle():
        return 'create'


class RelevarImportCsvContactData(ModelView):
    'Relevar CSV file'
    __name__ = 'gnuhealth.relevar.import_csv.contact_data'

    import_ = fields.Boolean('Import')

    patient_puid = fields.Char('Patient PUID',readonly=True)
    patient_name = fields.Char('Patient Name',readonly=True)
    patient_lastname = fields.Char('Patient Lastname',readonly=True)
    patient_dob = fields.Date('Date of birth', readonly=True)
    patient_gender = fields.Char('Gender',readonly=True)
    patient_qr = fields.Boolean('QR', readonly=True)
    patient_similarities =\
        fields.One2Many('gnuhealth.patient',None,'Patient similaties',readonly=True)
    check_assertion = fields.Boolean('Check assertion',
        help="Check if the patient data is good enough to import in case of similarities")
    patient_assertion = fields.Many2One('gnuhealth.patient','Patient assertion',
        help="Select one assertion existent on the database to those about to import",
        domain=[('id','in', Eval('patient_similarities'))])
    warning_icon = fields.Function(
        fields.Char('Check similarities'),'on_change_with_warning_icon')
    data_handle = fields.Selection ([
        ('create','Create Patient with app data'),
        ('update','Update Patient data with app data'),
        ('not_update','Not update data'),
        ],'Data handle',sort= False,
        states={
            'readonly': ~Eval('patient_assertion'),
            })

    @staticmethod
    def default_import_():
        return True

    @staticmethod
    def default_data_handle():
        return 'create'

    @fields.depends('patient_similarities','patient_assertion','check_assertion')
    def on_change_with_warning_icon(self,name=None):
        if self.patient_similarities and not self.check_assertion:
            return 'gnuhealth-warning'
        elif self.check_assertion:
            return ''
        return ''


class RelevarImportCsvWizard(Wizard):
    'Import Csv Start Wizard'
    __name__ = 'gnuhealth.relevar.import_csv.wizard'

    start = StateView('gnuhealth.relevar.import_csv.start',
                      'health_relevar_fiuner.relevar_import_csv_start_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Preview','prevalidate','tryton-ok',default=True),
                       ])
    prevalidate = StateTransition()
    preview = StateView('gnuhealth.relevar.import_csv.preview',
                        'health_relevar_fiuner.relevar_import_csv_preview_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Import','import_data','tryton-ok',True),
                        ])
    ask_import_anyway = StateTransition()
    import_data = StateAction('health.action_gnuhealth_patient_view')

    def transition_prevalidate(self):
        return 'preview'

    def default_preview(self, fields):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        DU = pool.get('gnuhealth.du') 
        HousingConditions = pool.get('gnuhealth.housing')
        parties = Party.search([('id','>',0),
                                ('is_person','=',True)])
        dus_record = DU.search([('id','>',0)])
        file_ = self.start.file_
        if not isinstance(file_, str):
             file_ = file_.decode('utf-8')
        file_ = StringIO(file_)
        contacts = []
        with file_ as csvfile:
            content = csv.reader(csvfile, delimiter=';')
            first_row = next(content)

            #personal data
            #'DNI', 'APELLIDO', 'NOMBRE', 'FECHA DE NACIMIENTO', 'SEXO',
            #'QR',
            personal_data = True if 'DNI' in first_row else False
            personal_data_sof = personal_data and first_row.index('DNI')

            #housing localization
            #'CALLE', 'NUMERO', 'COORDENADAS', 'ESTADO', 'GRUPO FAMILIAR', 'MENORES', 'MAYORES',
            housing_lczon_data = True if 'CALLE' in first_row else False
            housing_lczon_data_sof =\
                housing_lczon_data and first_row.index('CALLE')

            #housing conditions
            #'NUMERO CASA CARTOGRAFIA',0
            #'TIPO DE VIVIENDA', 1
            #'DUEÑO DE LA VIVIENDA', 2
            #'CANTIDAD DE PIEZAS',3
            #'LUGAR PARA COCINAR', 4
            #'USA PARA COCINAR...', 5
            #'MATERIAL PREDOMINANTE EN LAS PAREDES EXTERIORES', 6
            #'REVESTIMIENTO EXTERNO O REVOQUE',7
            #'MATERIAL DE LOS PISOS',8
            #'CIELORRASO', 9
            #'MATERIAL PREDOMINANTE EN LA CUBIERTA EXTERIOR DEL TECHO', 10
            #'AGUA',11
            #'ORIGEN AGUA', 12
            #'EXCRETAS', 13
            #'ELECTRICIDAD', 14
            #'GAS', 15
            #'ALMACENA AGUA DE LLUVIA', 16
            #'ÁRBOLES', 17
            #'BAÑO', 18
            #'EL BAÑO TIENE', 19
            #'NIEVE Y/O HIELO EN LA CALLE', 20
            #'PERROS SUELTOS', 21
            housing_conditions_data =\
                True if 'NUMERO CASA CARTOGRAFIA' in first_row else False
            housing_conditions_data_sof =\
                housing_conditions_data and first_row.index('NUMERO CASA CARTOGRAFIA')

            #contact mechanisms
            # 'TELEFONO FAMILIAR', 'CELULAR', 'FIJO', 'MAIL',
            contact_mechanisms_data =\
                True if 'TELEFONO FAMILIAR' in first_row else False
            contact_mechanisms_data_sof =\
                contact_mechanisms_data and first_row.index('TELEFONO FAMILIAR')

            #socioeconomics conditions
            # 'FACTORES DE RIESGO', 'EFECTOR', 'NOMBRE Y APELLIDO', 
            #'TELEFONO CONTACTO', 'PARENTEZCO', 'INGRESO Y OCUPACION', 'EDUCACION', 'VITAMINA D', 'DIA/MES/AÑO', 
            #'ULTIMO CONTROL', 'ENFERMEDAD ASOCIADA AL EMBARAZO', 'CERTIFICADO UNICO DE DISCAPACIDAD', 'TIPO DE 
            #DISCAPACIDAD', 'ACOMPAÑAMIENTO', 'TRASTORNOS EN NIÑOS', 'ADICCIONES', 'ACTIVIDADES DE OCIO', '¿DONDE 
            #REALIZA LAS ACTIVIDADES?', 'TIPO DE VIOLENCIA', 'MODALIDAD DE LA VIOLENCIA', 'TRASTORNOS MENTALES', 
            #'ENFERMEDADES CRONICAS', 'PLAN SOCIAL']
            socioeconomics_assesments_data =\
                True if 'FACTORES DE RIESGO' in first_row else False
            socioeconomics_assesments_sof =\
                socioeconomics_assesments_data and first_row.index('FACTORES DE RIESGO')

            dus = []
            survey_number = 1
            for row in content:
                #grab all party data
                contact = {}
                if personal_data:
                    index = personal_data_sof
                    ref = row[index+0]
                    patient_lastname = row[index+1]
                    patient_name = row[index+2]
                    patient_dob = row[index+3] and datetime.strptime(row[index+3],'%d/%m/%Y').date() or None
                    patient_gender = 'm' if row[index+4] == 'M' else 'f'
                    qr = True if row[index+5] == 'true' else False

                    #make variations of name
                    patient_name_variants =\
                        [patient_name.upper(),patient_name.lower(),patient_name.capitalize(),patient_name]
                    patient_name_separate = [x.split() for x in patient_name_variants]
                    patient_name_separate = [x for y in patient_name_separate for x in y]
                    patient_name_variants += patient_name_separate
                    #make variations of lastname
                    patient_lastname_variants =\
                        [patient_lastname.upper(),patient_lastname.lower(),patient_lastname.capitalize(),patient_lastname]
                    patient_lastname_separate = [x.split() for x in patient_lastname_variants]
                    patient_lastname_separate = [x for y in patient_lastname_separate for x in y]
                    patient_lastname_variants += patient_lastname_separate
                    patient_similarities = Patient.search(
                         ['OR',
                          [('name.name','in',
                           patient_name_variants),
                          ('name.lastname','in',
                           patient_lastname_variants),
                          ('name.dob','=',patient_dob),
                          ('name.gender','=',patient_gender)
                          ],
                          [('puid','=',ref)]
                          ])
                    contact.update({
                        'patient_puid': ref,
                        'patient_lastname': patient_lastname,
                        'patient_name': patient_name,
                        'patient_dob': patient_dob,
                        'patient_gender': patient_gender,
                        'patient_qr': qr,
                        'data_handle': 'create',
                        'check_assertion': True if not patient_similarities else False, 
                        'patient_similarities': [x.id for x in patient_similarities],
                        'warning_icon': 'gnuhealth-warning' if len(patient_similarities)>0 else '',
                        })
                #grab all du data
                du = {}
                if housing_lczon_data:
                    index = housing_lczon_data_sof
                    du_street = row[index+0] or None
                    try:
                        du_street_number = int(row[index+1])
                    except:
                        du_street_number = None
                    du_latitude = row[index+2] and row[index+2].split(' ')[0] or None
                    du_longitude = row[index+2] and row[index+2].split(' ')[1] or None
                    du_state = row[index+3]
                    du_family_group = row[index+4]
                    du_under_age_people = row[index+5]
                    du_elder_people = row[index+6]
                    #if du and there is personal data
                    if personal_data\
                        and du_latitude in [x['du_latitude'] for x in dus if 'du_latitude' in x]\
                        and du_longitude in [x['du_longitude'] for x in dus if 'du_longitude' in x]:
                        #TODO add patient_similarities dus
                        dus[-1]['du_people'].append(contact.copy())
                        dus[-1]['warning_icon'] = dus[-1]['warning_icon']\
                            or ('gnuhealth-warning' if contact['warning_icon'] == 'gnuhealth-warning' else '')

                    #else create a new du_data
                    else:
                        du_code =\
                            'RelevAR survey - ' + self.start.survey_date.strftime('%d-%m-%Y')\
                            + '-' +self.start.survey_responsable+'-'+str(survey_number)
                        survey_number +=1
                        #TODO add patient_similarities dus
                        du.update({
                            'du_code': du_code,
                            'du_street': du_street,
                            'du_street_number': du_street_number,
                            'du_latitude': du_latitude,
                            'du_longitude': du_longitude,
                            'du_people': [contact.copy()],
                            'du_list' : [x.id for x in dus_record if x.address_street == du_street and x.address_street_number == int(du_street_number)],
                            'du_data_handle': 'create',
                            'warning_icon':\
                                'gnuhealth-warning' if 'gnuhealth-warning' in contact['warning_icon'] == 'gnuhealth-warning' else ''
                            })
                        dus.append(du.copy())
                    if housing_conditions_data:
                        index = housing_conditions_data_sof
                        housing_minors = du_under_age_people or '0'
                        housing_adults = du_elder_people or '0'
                        type1 = ['CASA', 'DEPARTAMENTO', 'PENSION']
                        type2 = ['LOCAL NO CONSTRUIDO PARA VIVIENDA']
                        type3 = ['CASILLA','RANCHO']
                        type4 = ['VIVIENDA MOVIL','PERSONA VIVIENDO EN LA CALLE']
                        house = row[index+1]
                        housing_type =\
                            '1' if house in type1 else\
                            '2' if house in type2 else\
                            '3' if house in type3 else\
                            'another' if house in type4 else 'sin_dato'
                        housing_type_another =\
                            row[index+1] if housing_type == 'another' else ''
                        housing_bedrooms = row[index+3] and int(row[index+3]) or 0
                        kitchen = row[index+4] or None
                        housing_kitchen =\
                            'no' if kitchen == 'NO TIENE LUGAR O CUARTO PARA COCINAR' else 'si'
                        housing_kitchen_chars =\
                            'water_and_drain' if kitchen == 'CON INSTALACION DE AGUA Y DESAGUE' else\
                            'water_wo_drain' if kitchen == 'CON INSTALACION DE AGUA SIN DESAGUE' else\
                            'no_water' if kitchen == 'SIN INSTALACION DE AGUA' else\
                            'no_data'
                        gas = row[index+5] or None
                        housing_gas =\
                            'natural' if gas == 'GAS DE RED' else\
                            'envasado' if gas == 'GAS EN GARRAFA, EN TUBO O A GRANEL' else\
                            'ninguno'
                        housing_stove_feed =\
                            'grid_gas' if gas == 'GAS DE RED' else\
                            'tubed_gas' if gas == 'GAS EN GARRAFA, EN TUBO O A GRANEL' else\
                            'wood_coal' if gas == 'LEÑA O CARBON' else\
                            'electricity' if gas == 'electricity' else 'another'
                        walls = row[index+6]
                        housing_walls =\
                            'ladrillo' if walls == 'LADRILLO, PIEDRA, BLOQUE, HORMIGON' else\
                            'barro' if walls == 'ADOBE' else\
                            'chapa' if walls == 'CHAPA DE METAL O FIBROCEMENTO' else\
                            'madera' if walls == 'MADERA' else\
                            'paja' if walls == 'CHORIZO, CARTON, PALMA, PAJA SOLA O MATERIAL DE DESECHO' else\
                            'otro' if walls == 'OTRO' else 'sin_dato'
                        housing_walls_else = 'otro sin especificar' if housing_walls == 'otro' else ''
                        floors = row[index+8]
                        housing_floors =\
                            'ceramico' if floors == 'CERAMICA, BALDOSA, MOSAICO, MADERA O CEMENTO ALISADO' else\
                            'cemento' if floors == 'CARPETA, COMTRAPISO O LADRILLO FIJO' else\
                            'tierra' if floors == 'TIERRA O LADRILLO SUELTO' else 'otros'
                        housing_floor_else = 'otro sin especificar' if housing_floors == 'otros' else ''
                        roofs = row[index+10]
                        housing_roofs =\
                            'chapa' if roofs == 'CHAPA DE METAL' else 'otro'
                        housing_roofs_else =\
                            roofs if housing_roofs == 'otro' else ''
                        water_location = row[index+11]
                        water_source = row[index+12]
                        housing_water =\
                            'potable' if water_source == 'RED PUBLICA' else\
                            'profundas' if  water_source in\
                                ['PERFORACION CON BOMBA A MOTOR','PERFORACION CON BOMBA MANUAL','POZO SIN BOMBA'] else 'sin_dato'
                        housing_water_source =\
                            'public_grid' if water_source == 'RED PUBLICA' else\
                            'motor_pump' if water_source == 'PERFORACION CON BOMBA A MOTOR' else\
                            'manual_pump' if water_source == 'PERFORACION CON BOMBA MANUAL' else\
                            'well_no_pump' if water_source == 'POZO SIN BOMBA' else\
                            'no_data' if not water_source else 'other'
                        housing_water_source_other = water_source if housing_water_source == 'other' else ''
                        housing_water_location =\
                            'pipes_inside_house' if water_location == 'POR CAÑERIA DENTRO DE LA VIVIENDA' else\
                            'outside_house_inside_land' if water_location == 'FUERA DE LA VIVIENDA PERO DENTRO DEL TERRENO' else\
                            'outside_land' if water_location == 'FUERA DEL TERRENO' else 'no_data'
                        excretes = row[index+13]
                        housing_excretes=\
                            'cloacas' if excretes == 'A RED PUBLICA O CLOACA' else\
                            'letrina1' if excretes == 'A CAMARA SEPTICA Y POZO CIEGO' else\
                            'letrina2' if excretes == 'SOLO A POZO CIEGO' else\
                            'letrina3' if excretes == 'A HOYO, EXCAVACION EN LA TIERRA' else 'sin_dato'
                        housing_electricity =\
                            'si' if row[index+14] == 'SI' else\
                            'no' if row[index+14] == 'NO' else 'sin_dato'
                        bathroom = row[index+18]
                        bathroom_type = row[index+19]
                        housing_bathroom =\
                            'inside_house' if bathroom == 'DENTRO DE LA VIVIENDA' else\
                            'outside_house' if bathroom == 'FUERA DE LA VIVIENDA' else\
                            'no' if bathroom == 'NO TIENE' else 'no_data'
                        housing_bathroom_type =\
                            'toilet_w_button' if bathroom_type == 'INODORO CON BOTON, MOCHILA O CADENA' else\
                            'toilet_wo_button' if bathroom_type == 'INODORO SIN BOTON O SIN CADENA' else\
                            'letrine' if bathroom_type == 'POZO' else 'no_data'
                        housing_snow_ice =\
                            'yes' if row[index+20] == 'SI' else\
                            'no' if row[index+20] == 'NO' else 'no_data'
                        dus[-1]['housing_minors'] = housing_minors
                        dus[-1]['housing_adults'] = housing_adults
                        dus[-1]['housing_type'] = housing_type
                        dus[-1]['housing_type_another'] = housing_type_another
                        dus[-1]['housing_bedrooms'] = housing_bedrooms
                        dus[-1]['housing_kitchen'] = housing_kitchen
                        dus[-1]['housing_kitchen_chars'] = housing_kitchen_chars
                        dus[-1]['housing_gas'] = housing_gas
                        dus[-1]['housing_stove_feed'] = housing_stove_feed
                        dus[-1]['housing_walls'] = housing_walls
                        dus[-1]['housing_walls_else'] = housing_walls_else
                        dus[-1]['housing_floors'] = housing_floors
                        dus[-1]['housing_floor_else'] = housing_floor_else
                        dus[-1]['housing_roofs'] = housing_roofs
                        dus[-1]['housing_roofs_else'] = housing_roofs_else
                        dus[-1]['housing_water'] = housing_water
                        dus[-1]['housing_water_source'] = housing_water_source
                        dus[-1]['housing_water_source_other'] = housing_water_source_other
                        dus[-1]['housing_water_location'] = housing_water_location
                        dus[-1]['housing_excretes'] = housing_excretes
                        dus[-1]['housing_electricity'] = housing_electricity
                        dus[-1]['housing_bathroom'] = housing_bathroom
                        dus[-1]['housing_bathroom_type'] = housing_bathroom_type
                        dus[-1]['housing_snow_ice'] = housing_snow_ice
                if socioeconomics_assesments_data:
                    index = socioeconomics_assesments_sof
        return {
            'dus':dus,
            }

    def do_import_data(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        DU = pool.get('gnuhealth.du')
        HousingConditions = pool.get('gnuhealth.housing')
        patients_in_db = Patient.search([('id','>',0)])

        data = []
        for du_data in self.preview.dus:
            #prepare a du to get all the patients in the same house
            du_latitude = Decimal(du_data.du_latitude).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
            du_longitude = Decimal(du_data.du_longitude).quantize(Decimal('1.11111111111111'), rounding=ROUND_UP)
            du = None
            if du_data.du_data_handle == 'create':
                du = DU.create([{
                    'name': du_data.du_code,
                    'address_street': du_data.du_street,
                    'address_street_number': du_data.du_street_number,
                    'address_city': self.start.city,
                    'address_country': self.start.country.id,
                    'address_subdivision': self.start.subdivision.id,
                    'latitude': du_latitude,
                    'longitude': du_longitude,
                    }])
                du = du[0]
            elif du_data.du_data_handle == 'update':
                du = du_data.du_assertion
                du.name = du_data.du_code
                du.address_street = du_data.du_street
                du.address_street_number = du_data.du_street_number
                du.address_city = self.start.city
                du.address_country = self.start.country.id
                du.address_subdivision = self.start.subdivision.id
                du.latitude = du_data.du_latitude
                du.longitude = du_data.du_longitude
                #TODO du.housing_conditions = [x.id for y.housing_conditions in dus for x in y]
                #TODO du.dengue_survey = [x.id for y.dengue_survey in dus for x in y]
                du.save()
            elif du_data.du_data_handle == 'not_update':
                du = du_data.du_assertion
                du.name = du_data.du_code
                du.save()
            #no patient on the database has a du related
            for patient_data in du_data.du_people:
                if patient_data.data_handle == 'create':
                    party = Party.create([{
                        'ref': patient_data.patient_puid,
                        'name': patient_data.patient_name,
                        'lastname': patient_data.patient_lastname,
                        'dob': patient_data.patient_dob,
                        'gender': 'm' if patient_data.patient_gender in ['m','M'] else 'f',
                        'du': du.id,
                        'is_patient': True,
                        'is_person': True,
                        'fed_country': self.start.country.name[:3].upper(),
                        'addresses': [('create',[{
                            'street':\
                                du.address_street or '' + ' '+\
                                str(du.address_street_number) or '',
                            'country':\
                                du.address_country and du.address_country.id or self.start.country.id,
                            'subdivision':\
                                du.address_subdivision and du.address_subdivision.id or self.start.subdivision.id,
                            }])]
                        }])
                    patient = Patient.create([{
                        'name': party[0].id
                        }])
                    data.append(patient[0])
                #update party-patient data
                elif patient_data.data_handle == 'update':
                    patient = patient_data.patient_assertion
                    patient.name.ref = patient_data.patient_puid
                    patient.name.dob = patient_data.patient_dob
                    patient.name.gender =  'm' if patient_data.patient_gender in ['m','M'] else 'f'
                    patient.name.du = du.id
                    patient.name.addresses[0].street = du.address_street + ' '+str(du.address_street_number)
                    patient.save()
                    data.append(patient)
                elif patient_data.data_handle == 'not_update':
                    patient = patient_data.patient_assertion
                    patient.name.du = du.id
                    patient.save()
                    data.append(patient)

            #if there is a data on the housing conditions
            if du_data.housing_type:
                housing_conditions = {
                    'du': du.id,
                    'revision_date': self.start.survey_date,
                    'housing_type': du_data.housing_type,
                    'housing_type_another': du_data.housing_type_another,
                    'water': du_data.housing_water,
                    'water_location': du_data.housing_water_location,
                    'water_source': du_data.housing_water_source,
                    'water_source_other': du_data.housing_water_source_other,
                    'bathroom': du_data.housing_bathroom,
                    'bathroom_type': du_data.housing_bathroom_type,
                    'snow_ice': du_data.housing_snow_ice,
                    'excretes': du_data.housing_excretes,
                    'disposal': 'sin_dato',
                    'walls': du_data.housing_walls,
                    'walls_else': du_data.housing_walls_else,
                    'roofs': du_data.housing_roofs,
                    'roof_else': du_data.housing_roofs_else,
                    'floors': du_data.housing_floors,
                    'floor_else': du_data.housing_floor_else,
                    'housemates':\
                        (du_data.housing_adults and int(du_data.housing_adults))\
                        + (du_data.housing_minors and int(du_data.housing_minors)),
                    'minors': int(du_data.housing_minors),
                    'gas': du_data.housing_gas,
                    'stove_feed': du_data.housing_stove_feed,
                    'kitchen': du_data.housing_kitchen,
                    'kitchen_chars': du_data.housing_kitchen_chars,
                    'electricity': du_data.housing_electricity,
                    'bedrooms': du_data.housing_bedrooms,
                    }
                HousingConditions.create([housing_conditions])

        data = {'res_id': [d.id for d in data]}
        if len(data['res_id']) == 1:
            action['views'].reverse()
        return action, data
